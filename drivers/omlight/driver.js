"use strict";

const Driver = require("../common.js");

class Light extends Driver {
    _isValidOutput(config) {
        return config.module_type === "O" && config.type === 255;
    }
}

module.exports = new Light();
