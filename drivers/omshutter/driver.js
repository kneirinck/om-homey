"use strict";

const Driver = require("../common.js");

class Shutter extends Driver {
    _isValidOutput(config) {
        return config.module_type === "R";
    }
}

module.exports = new Shutter();
