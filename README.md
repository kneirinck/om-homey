# OM-Homey
This app adds support for OpenMotics devices in Homey.

Thanks to [www.openmotics.com](https://www.openmotics.com/).

# Changelog
* 0.0.1 - initial release