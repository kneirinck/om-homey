"use strict";

const request = require("request");
const AsyncLock = require("node-async-locks").AsyncLock;

var lock = new AsyncLock();

class Driver {
    constructor() {
        // a list of devices, with their 'id' as key
        // it is generally advisable to keep a list of
        // paired and active devices in your driver's memory.
        this._devices = {};
        this._token = undefined;

        this.init = this._onExportsInit.bind(this);
        this.added = this._onExportsAdded.bind(this);
        this.deleted = this._onExportsDeleted.bind(this);
        this.pair = this._onExportsPair.bind(this);

        // these are the methods that respond to get/set calls from Homey
        // for example when a user pressed a button
        this.capabilities = {
            onoff: {
                get: this._onExportsCapabilitiesOnoffGet.bind(this),
                set: this._onExportsCapabilitiesOnoffSet.bind(this)
            }
        };
    }

    // the `init` method is called when your driver is loaded for the first time
    _onExportsInit(devices_data, callback) {
        devices_data.forEach(this._initDevice.bind(this));

        callback();
    }

    // the `added` method is called is when pairing is done and a device has been added
    _onExportsAdded(device_data, callback) {
        this._initDevice(device_data);

        callback(null, true);
    }

    // the `delete` method is called when a device has been deleted by a user
    _onExportsDeleted(device_data, callback) {
        delete this._devices[device_data.id];

        callback(null, true);
    }

    // the `pair` method is called when a user start pairing
    _onExportsPair(socket) {
        socket.on("list_devices", (data, callback) => {
            this._getOutputs().then(outputs => {
                Homey.log("And there were outputs!");
                callback(null, outputs);
            });
        });
    }

    _onExportsCapabilitiesOnoffGet(device_data, callback) {
        let device = this._getDeviceByData(device_data);
        if (device instanceof Error) {
            return callback(device);
        }

        return callback(null, device.state.onoff);
    }

    _onExportsCapabilitiesOnoffSet(device_data, onoff, callback) {
        let device = this._getDeviceByData(device_data);
        if (device instanceof Error) {
            return callback(device);
        }

        device.state.onoff = onoff;

        // here you would use a wireless technology to actually turn the device on or off
        this._setOutputState(device, onoff);

        // also emit the new value to realtime
        // this produced Insights logs and triggers Flows
        this.realtime(device_data, "onoff", device.state.onoff);

        return callback(null, device.state.onoff);
    }

    // a helper method to add a device to the devices list
    _initDevice(device_data) {
        this._devices[device_data.id] = {
            state: {
                onoff: true
            },
            id: device_data.id
        };
    }

    // a helper method to get a device from the devices list by it's device_data object
    _getDeviceByData(device_data) {
        let device = this._devices[device_data.id];
        if (typeof device === "undefined") {
            return new Error("invalid_device");
        } else {
            return device;
        }
    }

    _getUrl(path) {
        return "https://" + Homey.manager("settings").get("ipaddress") +
            path;
    }

    _login() {
        return new Promise((resolve, reject) => {
            lock.enter((lockToken) => {
                if (this._token !== undefined) {
                    Homey.log("cached login");
                    lock.leave(lockToken);
                    resolve();
                    return;
                }
                Homey.log("login");
                request.post({
                    url: this._getUrl("/login"),
                    form: {
                        username: Homey.manager(
                            "settings").get(
                            "username"),
                        password: Homey.manager(
                            "settings").get(
                            "password")
                    },
                    strictSSL: false,
                    json: true
                }, (err, response, body) => {
                    if (err || response.statusCode !==
                        200) {

                        lock.leave(lockToken);
                        return reject();
                    }
                    this._token = body.token;
                    lock.leave(lockToken);
                    resolve();
                });

            });
        });
    }

    _getOutputs() {
        return new Promise(resolve => {
            if (this._token === undefined) {
                return this._login().then(this._getOutputs).then(
                    outputs => resolve(outputs));
            }
            request.post({
                url: this._getUrl(
                    "/get_output_configurations"),
                form: {
                    token: this._token
                },
                strictSSL: false,
                json: true
            }, (err, response, body) => {
                if (err) {
                    Homey.log(err);
                    return resolve([]);
                } else if (response.statusCode === 401 ||
                    response.statusCode ===
                    404) {
                    // Need to request a (new) token
                    this._token = undefined;
                    return this._login().then(this._getOutputs)
                        .then(outputs => resolve(outputs));
                } else if (response.statusCode !== 200) {
                    Homey.log(body);
                    return resolve([]);
                }

                let outputs = [];
                body.config.forEach(config => {
                    if (!this._isValidOutput(config)) {
                        return;
                    }
                    outputs.push({
                        name: config.name,
                        data: {
                            id: config.id
                        }
                    });
                });

                resolve(outputs);
            });
        });
    }

    _setOutputState(device, onoff) {
        return new Promise(resolve => {
            if (this._token === undefined) {
                return this._login().then(() => this._setOutputState(
                    device,
                    onoff))
                    .then(resp => resolve(resp));
            }
            request.post({
                url: this._getUrl("/set_output"),
                form: {
                    token: this._token,
                    id: device.id,
                    is_on: onoff
                },
                strictSSL: false,
                json: true
            }, (err, response, body) => {
                if (err) {
                    Homey.log(err);
                    return resolve(false);
                } else if (response.statusCode === 401 ||
                    response.statusCode ===
                    404) {
                    // Need to request a (new) token
                    this._token = undefined;
                    return this._login().then(() =>
                        this._setOutputState(
                            device,
                            onoff)).then(resp =>
                                resolve(resp));
                } else if (response.statusCode !== 200) {
                    Homey.log(body);
                    return resolve(false);
                }
                Homey.log(device.id + " done " + JSON.stringify(
                    body));

                resolve(body.success);
            });
        });
    }
}

module.exports = Driver;
