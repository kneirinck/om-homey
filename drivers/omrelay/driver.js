"use strict";

const Driver = require("../common.js");

class Relay extends Driver {
    _isValidOutput(config) {
        return config.module_type === "O" && config.type === 0;
    }
}

module.exports = new Relay();
